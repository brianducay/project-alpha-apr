Feature1	0/5
    Install dependencies
    Create your virtual environment for that directory:
        python -m venv .venv
    Activate your virtual environment using your specific terminal command:
        source ./.venv/bin/activate
    Upgrade pip with:
        python -m pip install --upgrade pip
    Install django
        pip install django
    Install black
    Install flake8
    Install djhtml


Feature2	0/7
    Set up the Django project and apps
    Create App # django-admin startproject
    Create apps accounts, projects, tasks
    istall apps in settings.py

Feature3	0/12
    The Project model	2
    Create View named 'Project'
        name | string	maximum length of 200 characters
        description ||	string	no maximum length
        members	|| many-to-many	Refers to the auth.User model, related name "projects"
    __str__

Feature4	0/1
    Registering Project in the admin	3


Feature5	0/9
    The Project list view	3
    register view url path "" name=list_projects
    register projects app as tracker path projects/

Feature6	0/2
    Default path redirect	5
    tracker urls.py -- redirect "" --> home

Feature7	0/10
    Login page	2
    reg LoginView in accounts urls
    create templates w a POST form
    ++ LOGIN_REDIRECT_URL = "login" to settings.py


Feature8	0/3
    Require login for Project list view	5, 7
    protect listview for logged in users only
    filter to members = user

Feature9	0/4
    Logout page	7
    import LogoutView
    register in URL patterns w "logout/"

    ++ LOGOUT_REDIRECT_URL = "login" to settings.py

Feature10	0/10
    Sign up page	2
    import UserCreationForm
    use create_user method to allow new user creation
    login function
    redirect to "home"
    make template with POST form

Feature11	0/20
    The Task model	3
        fields = name, stat_date, due_date, is_completed, project, assignee
        __str__

Feature12	0/1
    Registering Task in the admin	11

Feature13	1/15
    The Project detail view	12
    register URL "create/" name=create_project
    create template
    add tasks # on listview
    create link to project detail page

Feature14	1/11
    The Project create view	2
    view, filter by user, all field except completed
    redirect to detail page
    register URL "create/" name=create_project
    create template
    add link to list view


Feature15	0/13
    The Task create view	11
    view, filter by user, all field except completed
    register URL "create/" name=create_task
    register task app URL in tracker project
    create template


Feature16	0/7
    Show "My Tasks" list view	11
    Task list view --
        view, filter by asignee
        register URL "mine/" name=show_my_tasks
        create template

Feature17	0/11
    Completing a task	16
    Task update view --
        view,
        redirect to show_my_tasks,
        register URL "<int:pk>/complete/" name=complete_task
        copy paste button code


Feature18	0/1
    Markdownify	17
    install, register, apply to detail page

Feature19	0/2
    Navigation	5, 7, 9, 10, 16
    on base.html --
            if authenticated, projects, tasks, logout links
            if not, login, signup links

Last steps 30
    flake8, black, djhtml
